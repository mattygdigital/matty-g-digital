A #buzzword free, common sense digital marketer growing businesses online with ads, email, search engines, and social media. Work with an actual living, breathing person in Lindsay, Kawartha Lakes + Peterborough (not some cookie-cutter digital marketing agency).

Address: 549 Halter Road, Lindsay, ON K9V 4R4, Canada

Phone: 905-259-5718

Website: http://www.mattygdigital.com
